#ifndef _MAIN_H
#define _MAIN_H


#define TIMER0_PRELOAD	200
#define TIMER_TICKS		250
#define TIMER_POWERUP	600
#define BLINK_TICKS		100
#define MINUTES_MAX		 30
#define SEC_STEP_CW	 	 20
#define SEC_STEP_CCW	 5

#define EEPROM_ADR_MINUTES 0x01
#define EEPROM_ADR_SECONDS 0x02
#define EEPROM_ADR_RUNTIME 0x03



/* Portpins */
	/* Switches */
#define	SW_PORT			PORTD
#define SW_PIN			PIND
#define SW_DDR			DDRD
#define	SW_BUTTON		1
#define SW_ROTARY		4
#define SW_PHASE_A		3
#define SW_PHASE_B		5
#define SW_OPEN			6
	/* Outputs */
#define UV_LED_PORT		PORTB
#define UV_LED_DDR		DDRB
#define UV_LED			1
#define RED_LED_PORT	PORTD
#define RED_LED_DDR		DDRD
#define RED_LED			2
#define BEEPER_PORT		PORTD
#define BEEPER_DDR		DDRD
#define BEEPER			7
#define LCD_BL_PORT		PORTB
#define LCD_BL_DDR		DDRB
#define LCD_BL			2

/* program states */
#define STATE_IDLE			10		// Stand by
#define STATE_EXPOSE		20		// Belichtung läuft
#define STATE_PAUSE_RESUME	21		// Belichtung unterbrochen
#define STATE_PAUSE_ABORT	22		// Belichtung unterbrochen
#define STATE_ERR_OPEN		30		// Fehler Deckel geöffnet
#define STATE_ERR_TEMP		31		// Fehler Temperatur zu hoch
#define STATE_BEEP			40		// Temperaturgrenze einstellen
#define STATE_POWERUP		1		// Temperaturgrenze einstellen

#endif //#ifndef _MAIN_H
