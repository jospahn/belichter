#include <avr/io.h>
#include "lcd.h"

/*** Instruction Set ***/

#define LCD_CLR_SCR			0x01
#define LCD_HOME			0x02

#define LCD_ENTRY_MODE_SET	0x04
	#define LCD_MODE_DECREMENT	0x00
	#define LCD_MODE_INCREMENT	0x02
	#define LCD_MODE_NOSHIFT	0x00
	#define LCD_MODE_SHIFT		0x01

#define LCD_CONTROL			0x08
	/* further defines located in lcd.h as they
	 * may be used external with lcd_mode(char mode)
	 */

#define LCD_SET_SHIFT	0x10
	#define LCD_SHIFT_DISPLAY	0x04
	#define LCD_SHIFT_CURSOR	0x00
	#define LCD_SHIFT_RIGHT		0x02
	#define LCD_SHIFT_LEFT		0x00

#define LCD_FUNCTIONSET		0x20
	#define LCD_8BIT			0x10
	#define LCD_4BIT			0x00
	#define LCD_2LINE			0x08

#define LCD_SET_CGRAM		0x40
#define LCD_SET_DDRAM		0x80

/*** Delay times ***/

//#define F_MCLK		32768
#define LCD_ENABLE_US	60
#define LCD_WRITE_US	120
#define LCD_POWERUP_US	40000
#define LCD_CLRSCR_US	8000

/*** modul globale Variablen ***/
unsigned char LcdAdressCounter = LCD_LINE_1;

/************************************************************************************************
 * delay loop, delaying 4 cycles
 ***********************************************************************************************/
static inline void __delay_cycles(uint32_t __n)
{
	__asm__ __volatile__ (
			"1: sbiw %0,1" "\n\t"
			"brne 1b"
			: "=w"(__n)
			: "0"(__n)
		  );
}

void lcd_pulse_enable()
{
    LCD_OUT |= 1<<LCD_PIN_EN;			/* Setze ENABLE pin */
    __delay_cycles(LCD_ENABLE_US);	/* warte */
    LCD_OUT &= ~(1<<LCD_PIN_EN);		/* loesche ENABLE pin */
}

void lcd_send_byte (char databyte, char rs)
{
	LCD_OUT &= ~LCD_PIN_MASK;		/* clear data pins */
	if (rs) LCD_OUT |= 1<<LCD_PIN_RS;  /* Wenn Daten gesendet werden, muss RS gesetzt sein - Bei Kommandos nicht */
	LCD_OUT |= (databyte & 0xF0) >> 2;		/* Highnibble an port setzten */
	lcd_pulse_enable();				/* Enable Puls senden, damit das LCD die Daten übernimmt */

	LCD_OUT &= ~LCD_PIN_MASK;		/* clear data pins */
	if (rs) LCD_OUT |= 1<<LCD_PIN_RS;  /* Wenn Daten gesendet werden, muss RS gesetzt sein - Bei Kommandos nicht */
	LCD_OUT |= ((databyte & 0x0F)<<2);		/* Lownibble an port setzten */
	lcd_pulse_enable();				/* Enable Puls senden, damit das LCD die Daten übernimmt */
	__delay_cycles(LCD_WRITE_US);
	LCD_OUT &= ~LCD_PIN_MASK;		/* clear data pins */
}

void lcd_init (void)
{
	/* Port als Ausgang und low */
    LCD_DIR |= LCD_PIN_MASK;
	LCD_OUT &= ~LCD_PIN_MASK;		/* clear data pins */

    /* Warten, da das LCD eine groessere PowerUp Zeit braucht als der MSP430 */
    __delay_cycles(LCD_POWERUP_US);

    /* Als 8-Bit Befehl 0x20 senden um den 4-Bit Modus zu aktivieren */
    LCD_OUT |= LCD_FUNCTIONSET>>2;
    lcd_pulse_enable();
    /* Ab jetzt mit 4 bit arbeiten. Nochmals FUNCTION SET 4 Bit sowie ggf. weiter Attribute senden */
    lcd_send_byte( LCD_FUNCTIONSET
    				| LCD_4BIT
    				| LCD_2LINE
    			, 0);

    lcd_send_byte( LCD_CONTROL
    				| LCD_DISPLAY_ON
    			, 0);

    lcd_send_byte( LCD_CLR_SCR
    			, 0);

    __delay_cycles(LCD_CLRSCR_US);

    lcd_send_byte( LCD_ENTRY_MODE_SET
    				| LCD_MODE_INCREMENT
    				| LCD_MODE_NOSHIFT
    			, 0);
}

void lcd_clrscr(void)
{
	lcd_send_byte(LCD_CLR_SCR,0);	/* Display löschen */
	lcd_send_byte(LCD_HOME,0);		/* Cursor Home */
	LcdAdressCounter = LCD_LINE_1;	/* Adresscounter auch auf Homeposition */
    __delay_cycles(LCD_CLRSCR_US);
}

void lcd_gotoxy(unsigned char x, unsigned char y)
{
	/* Zeilenadresse setzten */
	switch (y) {
	case 0:
		LcdAdressCounter = LCD_LINE_1;
		/* Bei manchen 1x16 Displays ist der Adressraum der ersten Zeile geteilt,
		 * dies wird ggf. hier bearbeitet.
		 */
#ifdef LCD_LINE_1A
		if (x>7){
			LcdAdressCounter = LCD_LINE_1A;
			x -= 8;
		}
#endif
		break;
	case 1:
		LcdAdressCounter = LCD_LINE_2;
		break;
	case 2:
		LcdAdressCounter = LCD_LINE_3;
		break;
	case 3:
		LcdAdressCounter = LCD_LINE_4;
		break;
	}

	/* Spalte hinzufügen */
	LcdAdressCounter += x;
	/* Kommando senden */
	lcd_send_byte(LCD_SET_DDRAM | LcdAdressCounter,0);
}

void lcd_mode(char mode)
{
    lcd_send_byte( LCD_CONTROL
    				| mode
    			, 0);
}

void lcd_putc(char databyte)
{
	lcd_send_byte(databyte,1);
	LcdAdressCounter++;
#ifdef LCD_LINE_1A
		if (LcdAdressCounter == 0x08){
			LcdAdressCounter = LCD_LINE_1A;
			lcd_send_byte(LCD_SET_DDRAM | LcdAdressCounter,0);
		}
#endif
}

void lcd_puts(char * datastring)
{
	while (*datastring){
		lcd_send_byte(*datastring, 1);
		datastring++;
		LcdAdressCounter++;
#ifdef LCD_LINE_1A
		if (LcdAdressCounter == 0x08){
			LcdAdressCounter = LCD_LINE_1A;
			lcd_send_byte(LCD_SET_DDRAM | LcdAdressCounter,0);
		}
#endif
	}
}


void lcd_definechar(char cg_adress, char * character_array)
{
	uint8_t i;
	lcd_send_byte(LCD_SET_CGRAM | cg_adress, 0);
	for (i=0; i<8; i++){
		lcd_send_byte( *(character_array+i) , 1);
	}
}
