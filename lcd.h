#ifndef _LCD_H
#define _LCD_H

/*** display modes to be used with lcd_mode(); */
	#define LCD_DISPLAY_ON		0x04
	#define LCD_DISPLAY_OFF		0x00
	#define LCD_CURSOR_ON		0x02
	#define LCD_CURSOR_OFF		0x00
	#define LCD_BLINK_ON	 	0x01
	#define LCD_BLINK_OFF	 	0x00

/*** LCD Port Mapping ***/
#define     LCD_DIR               DDRC
#define     LCD_OUT              PORTC
#define     LCD_PIN_RS            1
#define     LCD_PIN_EN            0
#define     LCD_PIN_D4            2
#define     LCD_PIN_D5            3
#define     LCD_PIN_D6            4
#define     LCD_PIN_D7            5
#define     LCD_PIN_MASK  ((1<<LCD_PIN_RS)|(1<<LCD_PIN_EN)|(1<<LCD_PIN_D7)|(1<<LCD_PIN_D6)|(1<<LCD_PIN_D5)|(1<<LCD_PIN_D4))

/* Zeilen und Spaltenanzahl des Displays sowie Startadressen der Zeilen.
 * Bei manchen 1x16 Displays ist die Adressierung der zeile nicht fortlaufend, ab der Hälfte gilt eine neue Adresse.
 * In diesem Fall die Startadresse der zweiten Hälfte bei LCD_LINE_1A hinterlegen, sonst Zeile auskommentiere!
 */
#define 	LCD_LINE_1				0x00
#define 	LCD_LINE_1A				0x40
#define 	LCD_LINE_2				0x40
#define 	LCD_LINE_3				0x10
#define 	LCD_LINE_4				0x50


void lcd_init (void);
void lcd_clrscr(void);
void lcd_mode(char mode);
void lcd_gotoxy(unsigned char x, unsigned char y);
void lcd_putc(char databyte);
void lcd_puts(char * datastring);
void lcd_definechar(char cg_adress, char * character_array);

#endif /* _LCD_H */
