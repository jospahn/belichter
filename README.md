Belichter
=========
Steuerung für ein Platinen Belichtungsgerät mit UV LEDs

+ Steuerung der Belichtungsdauer
+ Einstellung der Belichtungszeit mit Drehencoder und LCD
+ Persistente Speicherung der letzten Belichtungszeit als Preset im EEProm
+ Belichtungsvorgang und Zeit kann pausiert werden
+ Überwachung des Deckels -> Abschaltung der UV LEDs bei offenem Deckel
+ Temeperaturüberwachung
+ Buzzeransteuerung bei Belichtungsende
