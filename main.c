/************************************************************************************************
 * 	Projekt:		LED UV Belichtungsgerät
 * 	Autor:			Jan-Ole Spahn DB8JO
 * 	MCU:			Atmel ATmega 8
 * 	Beschreibung:	Ansteurung der LEDs mit Timer, Deckelüberwachung und Temperaturüberwachung
 * 	Version:		1.1, 22.7.2013, 2284 Bytes		- Deckelüberwachung implementiert
 * 	        		1.0, 21.7.2013, 2118 Bytes
 ************************************************************************************************/
 												
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/eeprom.h>
#include <stdlib.h>
#include "main.h"
#include "lcd.h"
#include "customchars.h"

/*** Variablen zur Tastendetektion ***/
volatile int8_t key_result = 0, key_release = 0, enc_result = 0;


/*** Prototypen ***/
uint8_t get_key_press( uint8_t key_mask );
uint8_t get_key_release( uint8_t key_mask );
int8_t encode_read(void);         // Encoder auslesen
void print_time(int8_t min, int8_t sec, int8_t x, int8_t y);
void print_runtime(uint32_t runtime);

/*** Hauptprogramm ***/
int main(void)
{
	uint8_t next_state = STATE_POWERUP, state = STATE_POWERUP;	// Der Zustand des Zustandsautomaten
	uint8_t statechange;										// Nachricht, das der zsuatnd gerade gewechselt hat
	uint16_t timer_ticks = TIMER_POWERUP;						// Zählen der Timer OVF, z.B. für den Sekundentakt
	uint16_t blink_ticks = BLINK_TICKS;							// Timer OVF zählen zum Blinken
	uint32_t runtime;											// Die Betreibszeit der LEDs, wird aus EEprom geladen und gespeichert
	int8_t encoder;												// Zum Speichern des gelsensn encoderwertes

	struct time {
		int8_t minutes;
		int8_t seconds;
	};

	struct time countdown;			// countdown während des Belichtens
	struct time countdown_preload;	// countdown Voreinstellung


	/* init ports */
		/* Set outputs */
	LCD_BL_DDR |= 1 << LCD_BL;
	RED_LED_DDR |= 1 << RED_LED;
	BEEPER_DDR |= 1 << BEEPER;
	UV_LED_DDR |= 1 << UV_LED;
		/* Set input pull up*/
	SW_PORT |= (1<<SW_BUTTON);
	SW_PORT |= (1<<SW_OPEN);


	/* Init LCD */
	LCD_BL_PORT |= 1 << LCD_BL;	// BL on
	lcd_init();
	/* custom chars im CG-Ram des LCD anlegen */
	lcd_definechar(0x40, &customchar[0][0]);
	lcd_definechar(0x48, &customchar[1][0]);
	lcd_definechar(0x50, &customchar[2][0]);
	lcd_definechar(0x58, &customchar[3][0]);
	lcd_definechar(0x60, &customchar[4][0]);
	lcd_definechar(0x68, &customchar[5][0]);

	lcd_gotoxy(0,0);
	lcd_puts("ULB v1.1");

	/* Daten aus EEPROM laden */
	countdown_preload.minutes = eeprom_read_byte((uint8_t *) EEPROM_ADR_MINUTES);
	countdown_preload.seconds = eeprom_read_byte((uint8_t *)EEPROM_ADR_SECONDS);
	runtime = eeprom_read_dword((uint32_t *)EEPROM_ADR_RUNTIME);

	/* Wenn beim Start der taster gedrückt wird, Defaultwerte laden */
	if (!(SW_PIN & (1<<SW_BUTTON))){
		runtime = 0;
		countdown_preload.minutes = 6;
		countdown_preload.seconds = 0;
		while (!(SW_PIN & 1<<SW_BUTTON));
	}

	/* Betriebsstunden ausgeben */
	print_runtime(runtime);

	/* timer 0, 2ms by overflow vector */
	TCCR0 |= (1<<CS00) | (1<<CS01);		//CPU / 64
	TCNT0 = TIMER0_PRELOAD;
	TIMSK |= 1<<TOIE0;

	sei();

	/* select sleep mode to be used later */
	set_sleep_mode (SLEEP_MODE_IDLE);

	for (;;) {
		cli();
		encoder = encode_read();		// Drehencoder auselesen

		/* Zustandautomat */
		switch (state){

		/* Programmstart, Verzögerung zum Anzeigen des Startdisplays (Betriebsstunden) */
		case STATE_POWERUP:
			timer_ticks--;
			if (!timer_ticks){
				timer_ticks = TIMER_TICKS;
				next_state = STATE_IDLE;
				statechange = 1;
			}
			break;
		/* Standby, hier ist die Countdowneinstellung möglich und die Belichtung kann gestartet werden */
		case STATE_IDLE:
			if (statechange){
				statechange = 0;
				lcd_clrscr();
				lcd_gotoxy(0,0);
				lcd_putc(3);	// Up /Dwn Symbol anzeigen
			}
			/* Bei Drehung des encoders, Countdown inkrementiern ode dekrementieren,
			 * es sind unterschiedlich Schrittweiten für links und rechtsdrehung möglich.
			 * Diese sind durch SEC_STEP_CW und SEC_STEP_CW fest gelgt.
			 */
			if (encoder < 0) {
				countdown_preload.seconds += (encoder * SEC_STEP_CCW);
				if (countdown_preload.seconds < 0) {
					countdown_preload.seconds = 60 - SEC_STEP_CCW;
					countdown_preload.minutes--;
					if (countdown_preload.minutes < 0)
						countdown_preload.minutes = MINUTES_MAX;
				}
			} else if (encoder > 0) {
				countdown_preload.seconds += (encoder * SEC_STEP_CW);
				if (countdown_preload.seconds >= 60){
					countdown_preload.seconds = 0;
					countdown_preload.minutes++;
					if (countdown_preload.minutes > MINUTES_MAX)
						countdown_preload.minutes = 0;
				}
			}
			/* Dei aktualiesierte Countdwonzeit ausgeben */
			print_time(countdown_preload.minutes, countdown_preload.seconds, 2 ,0);
			/* Wenn die Taste gedrückt wurde, die Belichtung starten */
			if (get_key_press(1<<SW_BUTTON)) {
				//if ((SW_PORT & (1<<SW_OPEN))){
					countdown = countdown_preload;		// Cuntdown Voreinstellung übernehmen
					next_state = STATE_EXPOSE;
					statechange = 1;
				//}
			}
			break;
		/* Belichtung: UV Leds sowie Taster LED werden angeschaltete, der Countown wird im Sekundentakt runtergezählt.
		 * Der Countdown Startwert wird ins EEprom geschrieben, damit er beim nächsten Start zur Verfügung steht.
		 * Durch Tastendruck wird die Belichtung pausiert und kann dann fortgesetzt oder abgebrochen werden.
		 */
		case STATE_EXPOSE:
			if (statechange){	// if just entering this state
				statechange = 0;
				lcd_clrscr();
				lcd_gotoxy(0,0);
				lcd_putc(0);
				print_time(countdown.minutes, countdown.seconds, 2, 0);
				RED_LED_PORT |= 1<<RED_LED;
				UV_LED_PORT |= 1<<UV_LED;
				/* Countdown in das EEprom schreiben, damit die Zeit beim nächsten programmstart übernommen wird */
				eeprom_update_byte ((uint8_t *)EEPROM_ADR_MINUTES, countdown.minutes);
				eeprom_update_byte ((uint8_t *)EEPROM_ADR_SECONDS, countdown.seconds);
			}
			/* Timer OVF Interrupts zählen um eine nSkeundentakt zu erhalten
			 * Wenn eine Sekunde um ist, Countdown runterzählen. Wenn Countdown
			 * abgelaufen ist, Belichtung beenden
			 */
			timer_ticks--;
			if (!timer_ticks) {
				timer_ticks = TIMER_TICKS;
				if (!countdown.seconds){
					if (!countdown.minutes){
						next_state = STATE_BEEP;	// Belichtunge beeden
					} else {
						countdown.seconds = 59;
						countdown.minutes--;
					}
				} else {
					countdown.seconds--;
				}
				print_time(countdown.minutes, countdown.seconds, 2, 0);
			}
			/* Bei Tastendruck Belichtung pausieren */
			if (get_key_press(1<<SW_BUTTON)) next_state = STATE_PAUSE_RESUME;
			if (get_key_press(1<<SW_OPEN)) next_state = STATE_ERR_OPEN;
			/* wenn der Countdown abeglaufen ist, wurde next_state gesetzt um den Countdown zu beenden oder wenn der
			 * Tastre gedrückt wurde, wurde next_state gesetzt um in den Zustand Pause zu wechseln.
			 * In Beiden Fällen die LEDs ausschalten.
			 */
			if (next_state != state){
				statechange = 1;
				RED_LED_PORT &= ~(1<<RED_LED);
				UV_LED_PORT &= ~(1<<UV_LED);
			}
			break;
		/* Wird nach Ablauf der Belichtung eine Seklunde aufgerufen. Während dessen summt der Summer und die Betriebsstunden
		 * der LED's werden im EEprom aktualisiert.
		 */
		case STATE_BEEP:
			if (statechange){
				statechange = 0;
				BEEPER_PORT |= 1<<BEEPER;
				runtime += countdown_preload.minutes * 60 + countdown_preload.seconds;
				eeprom_write_dword((uint32_t *)EEPROM_ADR_RUNTIME, runtime);
			}
			timer_ticks--;
			if (!timer_ticks) {
				timer_ticks = TIMER_TICKS;
				next_state = STATE_IDLE;
				BEEPER_PORT &= ~(1<<BEEPER);
				statechange = 1;
			}
			break;
		/* Belichtung ist pausiert, Tastendruck führt zur Fortsetzung.
		 * Drehen des Encoders wechselt in den Pausenzustand in dem Tastendruck zum Abbrcuh führt */
		case STATE_PAUSE_RESUME:
			if (statechange){
				statechange = 0;
				lcd_gotoxy(0,0);
				lcd_putc(1);
				lcd_gotoxy(0,1);
				lcd_puts("    [ ] ");
				lcd_gotoxy(2,1);
				lcd_putc(2);
				lcd_gotoxy(5,1);
				lcd_putc(0);
				blink_ticks = BLINK_TICKS;
			}
			blink_ticks--;
			if (!blink_ticks){
				blink_ticks = BLINK_TICKS;
				RED_LED_PORT ^= 1<<RED_LED;
			}
			if (encoder) next_state = STATE_PAUSE_ABORT;
			if (get_key_press(1<<SW_BUTTON)) next_state = STATE_EXPOSE;
			if (next_state != state) statechange = 1;
			break;
		/* Belichtung ist pausiert, Tastendruck führt zum Abbruch
		 * Drehen des Encoders wechselt in den Pausenzustand in dem Tastendruck zur Fortsetzung führt */
		case STATE_PAUSE_ABORT:
			if (statechange){
				statechange = 0;
				lcd_gotoxy(0,0);
				lcd_putc(1);
				lcd_gotoxy(0,1);
				lcd_puts(" [ ]    ");
				lcd_gotoxy(2,1);
				lcd_putc(2);
				lcd_gotoxy(5,1);
				lcd_putc(0);
			}
			blink_ticks--;
			if (!blink_ticks){
				blink_ticks = BLINK_TICKS;
				RED_LED_PORT ^= 1<<RED_LED;
			}
			if (encoder) next_state = STATE_PAUSE_RESUME;
			if (get_key_press(1<<SW_BUTTON)) {
				next_state = STATE_IDLE;
				RED_LED_PORT &= ~(1<<RED_LED);
				runtime += countdown_preload.minutes * 60 - countdown.minutes * 60;
				runtime += countdown_preload.seconds - countdown.seconds;
				eeprom_write_dword((uint32_t *)EEPROM_ADR_RUNTIME, runtime);

			}
			if (next_state != state) statechange = 1;
			break;
		case STATE_ERR_OPEN:
			if (statechange){
				statechange = 0;
				lcd_clrscr();
				lcd_puts("Fehler:  Deckel");
				blink_ticks = BLINK_TICKS;
			}
			blink_ticks--;
			if (!blink_ticks){
				blink_ticks = BLINK_TICKS;
				RED_LED_PORT ^= 1<<RED_LED;
			}
			if (get_key_release(1<<SW_OPEN)) {
				next_state = STATE_EXPOSE;
				statechange = 1;
			}
			break;
		}

		/* Zustandswechsle übernehmen, Interruts aktivieren und schlafen legen */
		state = next_state;
		sei();
		sleep_enable();
		sleep_cpu();
	}
}

/* Timer 0 Overflow Intzerrupt. Zeitbasis, Tastenentprellung unde Drehencoder auswertung */
ISR (TIMER0_OVF_vect)
{
    static uint8_t key_a=0, key_b=0;	// zur Tastenenprellung
    static uint8_t release_a=0, release_b=0xFF;	// zur Tastenenprellung
    uint8_t key_temp;

    static uint8_t enc_last;
    uint8_t enc_temp = 0;

    /*** Drehencoder auswerten ***/
    	/* Die Beiden Encoderpins einlesen und als LSB Bits von enc_temp merken */
    enc_temp |= (SW_PIN & (1 << SW_PHASE_A)) >> SW_PHASE_A;
    enc_temp |= (SW_PIN & (1 << SW_PHASE_B)) >> (SW_PHASE_B -1);

    /* Bei jeder Rastung sind beide Phasen high = II = 3
     * Der Encoder wird nur dan n ausgewertet */
    if (enc_temp == 3) {
    	if (enc_last == 1) enc_result = 1;		// Wenn das vorherige Bitmuster 0I war, gab es eine Rechtsdrehung */
    	if (enc_last == 2) enc_result = -1;		// Wenn dad vorherige Bitmuster I0 war, gab es eine Linksdrehung */
    }
    enc_last = enc_temp;

    /*** Tasten auswerten ***/
    key_temp = ~SW_PIN;
    key_result |= key_a & key_temp;
    key_a = key_temp & ~key_b;
    key_b = key_temp;

    key_temp = ~key_temp;
    key_release |= release_a & key_temp;
    release_a = key_temp & ~release_b;
    release_b = key_temp;



}

int8_t encode_read(void)         // Encoder auslesen
{
  int8_t val;
  val = enc_result;
  enc_result = 0;
  return val;
}

uint8_t get_key_press( uint8_t key_mask )
{
	key_mask &= key_result;
	key_result ^= key_mask;
	return key_mask;
}

uint8_t get_key_release( uint8_t key_mask )
{
	key_mask &= key_release;
	key_release ^= key_mask;
	return key_mask;
}

void print_time(int8_t min, int8_t sec, int8_t x, int8_t y)
{
	lcd_gotoxy(x, y);
	lcd_putc(min / 10 + '0');
	lcd_putc(min % 10 + '0');
	lcd_putc(':');
	lcd_putc(sec / 10 + '0');
	lcd_putc(sec % 10 + '0');
}

void print_runtime(uint32_t runtime)
{
	uint8_t h, m, s;
	h = runtime /  3600;
	m = runtime % 3600 / 60;
	s = (runtime % 3600) % 60;
	lcd_putc(h / 10 + '0');
	lcd_putc(h % 10 + '0');
	lcd_putc(':');
	lcd_putc(m / 10 + '0');
	lcd_putc(m % 10 + '0');
	lcd_putc('.');
	lcd_putc(s / 10 + '0');
	lcd_putc(s % 10 + '0');
}
